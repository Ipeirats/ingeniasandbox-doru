#include "StdAfx.h"
#include "Movil.h"

Movil::Movil(double _x, double _y)
	: x(_x)
	, y(_y)
{
}

Movil::~Movil(void)
{
}

void Movil::simular(double inc_t)
{
	y = y + 3*inc_t;
}